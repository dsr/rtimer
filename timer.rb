require 'thor'
require 'sequel'

DB = Sequel.connect("sqlite://timers.db")

class Timer < Sequel::Model
end

class RTimer < Thor
  
  desc "init", "Create the Sqlite database timers.db"
  def init()
    DB.create_table :timers do
      primary_key :id
      String :name
      Date :started
      Date :stopped
      Float :total_time
    end
    puts "Created timers.db"
  end

  desc "create TIMER", "Create and start TIMER"
  def create(timer)
    ds = DB[:timers]
    ds.insert(name: timer, started: Time.now)
    puts "Created #{timer}"
  end
  
  desc "stop TIMER", "Stop TIMER"
  def stop(timer)
    ds = DB[:timers]
    updated_timer = ds.filter(:name => timer, :stopped => nil).update(:stopped => Time.now)
    if updated_timer == 0
      puts "Timer #{timer} has already been stopped." if updated_timer == 0
    else
      puts "Stopped timer #{timer}"
    end
  end
end

RTimer.start(ARGV)
